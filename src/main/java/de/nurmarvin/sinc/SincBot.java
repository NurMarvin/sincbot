package de.nurmarvin.sinc;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mewna.catnip.Catnip;
import com.mewna.catnip.entity.channel.TextChannel;
import com.mewna.catnip.entity.guild.Guild;
import com.mewna.catnip.shard.DiscordEvent;
import de.nurmarvin.sinc.command.commands.developer.RoleIdCommand;
import de.nurmarvin.sinc.command.commands.moderation.WarnCommand;
import de.nurmarvin.sinc.manager.CommandManager;
import de.nurmarvin.sinc.manager.PunishmentManager;
import de.nurmarvin.sinc.manager.UserProfileManager;
import de.nurmarvin.sinc.manager.impl.MySQLPunishmentManager;
import de.nurmarvin.sinc.manager.impl.MySQLUserProfileManager;
import de.nurmarvin.sinc.manager.impl.SimpleCommandManager;
import de.nurmarvin.sinc.manager.impl.SimpleMySQLManager;
import de.nurmarvin.sinc.utils.Settings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public final class SincBot {
    public static final String VERSION = "0.1-DEV";

    private static final Logger LOGGER = LoggerFactory.getLogger(SincBot.class);
    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();
    private static SincBot instance;
    private final File settingsFile = new File("settings.json");
    private Settings settings;
    private Catnip catnip;

    private SimpleMySQLManager mySQLManager;
    private PunishmentManager punishmentManager;
    private UserProfileManager userProfileManager;
    private CommandManager commandManager;

    public static void main(String[] args) {
        try {
            new SincBot();
        } catch (IOException e) {
            LOGGER.error("An unhandled error occurred", e);
        }
    }

    private SincBot() throws IOException {
        instance = this;

        if (this.settingsFile.exists())
            this.loadSettings();
        else {
            this.settings = new Settings();
            this.saveSettings();
            LOGGER.info("Created config. Please insert discord token.");
            System.exit(0);
        }

        this.mySQLManager = new SimpleMySQLManager();
        this.commandManager = new SimpleCommandManager();
        this.punishmentManager = new MySQLPunishmentManager();
        this.userProfileManager = new MySQLUserProfileManager();

        this.commandManager.registerCommand(new WarnCommand());
        this.commandManager.registerCommand(new RoleIdCommand());

        Catnip.catnipAsync(this.settings.connectionSettings().token()).thenAccept(catnip -> {
            this.catnip = catnip;

            this.catnip.on(DiscordEvent.MESSAGE_CREATE, commandManager::handle);
            this.catnip.connect();
        });
    }

    public static SincBot instance() {
        return instance;
    }

    private void saveSettings() throws IOException {
        FileWriter fileWriter = new FileWriter(this.settingsFile);
        fileWriter.write(this.gson.toJson(this.settings));
        fileWriter.close();
    }

    private void loadSettings() throws FileNotFoundException {
        this.settings = this.gson.fromJson(new FileReader(this.settingsFile), Settings.class);
    }

    public Settings settings() {
        return settings;
    }

    public SimpleMySQLManager mySQLManager() {
        return mySQLManager;
    }

    public UserProfileManager userProfileManager() {
        return userProfileManager;
    }

    public PunishmentManager punishmentManager() {
        return punishmentManager;
    }

    public CommandManager commandManager() {
        return commandManager;
    }

    public Catnip catnip() {
        return catnip;
    }

    public Guild guild() {
        return this.catnip.cache().guild(this.settings().guildSettings().guildId());
    }

    public TextChannel modLogChannel() {
        return this.guild().textChannel(this.settings().guildSettings().modLogChannelId());
    }
}
