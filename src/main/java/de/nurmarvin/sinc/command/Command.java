package de.nurmarvin.sinc.command;

import com.google.common.base.Joiner;
import com.google.common.collect.Maps;
import com.mewna.catnip.entity.message.Message;
import com.mewna.catnip.entity.util.Permission;
import de.nurmarvin.sinc.SincBot;
import de.nurmarvin.sinc.utils.Embeds;
import de.nurmarvin.sinc.utils.Settings;

import java.util.*;

public abstract class Command
{
    private String name;
    private Permission requiredPermission;
    private HashMap<String, Command> subCommands;
    private Command parentCommand;
    private CommandLevel commandLevel;
    private String description;
    private String[] aliases;
    private String usage;

    public Command(String name, String... aliases) {
        this(name, CommandLevel.USER, aliases);
    }

    public Command(String name, CommandLevel commandLevel, String... aliases) {
        this(name, commandLevel == CommandLevel.SUPERMOD ? Permission.ADMINISTRATOR :
                   (commandLevel == CommandLevel.MODERATOR ? Permission.BAN_MEMBERS : null), commandLevel, aliases);
    }

    public Command(String name, Permission requiredPermission, String... aliases) {
        this(name, requiredPermission, CommandLevel.USER, aliases);
    }

    public Command(String name, Permission requiredPermission, CommandLevel commandLevel, String... aliases)
    {
        this.name = name;
        this.requiredPermission = requiredPermission;
        this.subCommands = Maps.newHashMap();
        this.description = "Command description not defined";
        this.usage = "";
        this.commandLevel = commandLevel;
        this.aliases = aliases;
    }

    public void addSubCommand(Command command)
    {
        command.parentCommand(this);

        this.subCommands.put(command.name.toLowerCase(), command);

        for(String alias : command.aliases)
            this.subCommands.put(alias.toLowerCase(), command);
    }

    public void preExecute(final CommandContext commandContext) throws CommandException {
        Message message = commandContext.message();

        if(this.requiredPermission != null &&
           !Objects.requireNonNull(message.member(), "Member can not be null").hasPermissions(requiredPermission)) {
            CommandExceptions.missingPermissions(commandContext, requiredPermission);
            return;
        }

        if(commandLevel == CommandLevel.DEVELOPER && !SincBot.instance().settings().botSettings().owner().equals(
                Objects.requireNonNull(message.member(), "Member can not be null").id()))
            Embeds.commandException(commandContext, new CommandException("This command can only be run by my master Marvin."));

        if(commandContext.hasArgs() && commandContext.arg(0).asString().equals("help")) {
            Embeds.usage(commandContext);
            return;
        }

        if(commandContext.args().length > 0 && subCommands.containsKey(commandContext.arg(0).asString().toLowerCase())) {
            Command subCommand = subCommands.get(commandContext.arg(0).asString().toLowerCase());

            if(commandContext.hasPermissions(subCommand.requiredPermission)) {
                String label = commandContext.arg(0).asString();

                String[] args = Arrays.copyOfRange(commandContext.args(), 1, commandContext.args().length);

                CommandContext newCommandContext = new CommandContext(message, this, label, args,
                                                                   Joiner.on(" ").join(args));

                subCommand.preExecute(newCommandContext);
            }
            return;
        }

        execute(commandContext);
    }

    public Command parentCommand() {
        return parentCommand;
    }

    public CommandLevel commandType() {
        return commandLevel;
    }

    public void commandType(CommandLevel commandLevel) {
        this.commandLevel = commandLevel;
    }

    private void parentCommand(Command parentCommand) {
        this.parentCommand = parentCommand;
    }

    public Permission requiredPermission() {
        return requiredPermission;
    }

    public void runSubCommand(String name, CommandContext commandContext) throws CommandException {
        if(subCommands.containsKey(name)) {
            Command subCommand = this.subCommands.get(name);

            String label = commandContext.args()[0];

            String[] args = Arrays.copyOfRange(commandContext.args(), 1, commandContext.args().length);

            subCommand.execute(new CommandContext(commandContext.message(), subCommand, label, args,
                                                  Joiner.on(" ").join(args)));
        }
    }

    public String usage() {
        return Settings.instance().botSettings().prefix()
               + (this.parentCommand != null ? this.parentCommand.name + " " : "")
               + this.name + (!this.usage.isEmpty() ? " " + this.usage : "") + " - " + this.description;
    }

    public HashMap<String, Command> subCommands() {
        return subCommands;
    }

    public String name() {
        return name;
    }

    public void subCommands(HashMap<String, Command> subCommands) {
        this.subCommands = subCommands;
    }

    public String[] aliases() {
        return aliases;
    }

    public void description(String description) {
        this.description = description;
    }

    public void usage(String usage) {
        this.usage = usage;
    }

    public String description() {
        return description;
    }

    public abstract void execute(CommandContext commandContext) throws CommandException;

    public void onRegister() {}
}
