package de.nurmarvin.sinc.command;

import com.mewna.catnip.entity.util.Permission;
import de.nurmarvin.sinc.utils.Embeds;

import java.util.Arrays;
import java.util.stream.Collectors;

public final class CommandExceptions {
    public static void missingPermissions(CommandContext commandContext, Permission... permission) {
        Embeds.commandException(commandContext, new CommandException(
                String.format("You don't have the required permissions to run this command." +
                              "\nYou are missing the following permission(s): `%s`",
                              Arrays.stream(permission).map(Permission::permName)
                                    .collect(Collectors.joining(", ")))));
    }
}
