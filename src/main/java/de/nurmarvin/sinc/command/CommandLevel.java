package de.nurmarvin.sinc.command;

public enum CommandLevel {
    DEVELOPER,
    SUPERMOD,
    MODERATOR,
    USER
}
