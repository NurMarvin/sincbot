package de.nurmarvin.sinc.command.commands.developer;

import com.mewna.catnip.entity.guild.Role;
import de.nurmarvin.sinc.command.Command;
import de.nurmarvin.sinc.command.CommandContext;
import de.nurmarvin.sinc.command.CommandException;
import de.nurmarvin.sinc.command.CommandLevel;
import de.nurmarvin.sinc.utils.Embeds;
import gg.amy.catnip.utilities.FinderUtil;

public class RoleIdCommand extends Command {
    public RoleIdCommand() {
        super("roleid", CommandLevel.DEVELOPER);
        this.description("Gets the role id of the provided role");
        this.usage("<role>");
    }

    @Override
    public void execute(CommandContext commandContext) throws CommandException {
        String roleName = commandContext.arg(0).asString();

        Role role = FinderUtil.findRoles(roleName, commandContext.guild()).stream()
                              .findFirst().orElseThrow(() -> new CommandException(
                        String.format("Can't find role with the provided name `%s`", roleName)));

        commandContext.send(Embeds.normalEmbed(commandContext.member())
              .title("Success")
              .description(String.format("Role: %s\nRole ID: %s", role.name(), role.id())).build());
    }
}
