package de.nurmarvin.sinc.command.commands.moderation;

import com.mewna.catnip.entity.builder.EmbedBuilder;
import com.mewna.catnip.entity.guild.Member;
import com.mewna.catnip.entity.user.User;
import de.nurmarvin.sinc.SincBot;
import de.nurmarvin.sinc.command.Command;
import de.nurmarvin.sinc.command.CommandContext;
import de.nurmarvin.sinc.command.CommandException;
import de.nurmarvin.sinc.command.CommandLevel;
import de.nurmarvin.sinc.profile.Punishment;
import de.nurmarvin.sinc.profile.impl.MySQLPunishment;

public class WarnCommand extends Command {
    public WarnCommand() {
        super("warn", CommandLevel.MODERATOR);
        this.usage("@user reason");
        this.description("Warns a user");
    }

    @Override
    public void execute(CommandContext commandContext) throws CommandException {
        Member member = commandContext.arg(0).asMember();
        String reason = commandContext.arg(1).asString();

        commandContext.message().react("✅").thenAccept(ignored ->
                member.createDM().thenAccept(dmChannel -> dmChannel
                .sendMessage(new EmbedBuilder().title("You have received a warning.")
                                               .field("Reason", reason, false)
                                               .build())
                .handle((message, throwable) -> {
                    this.warn(member, reason, commandContext.user());
                    return null;
                })));
    }

    private void warn(Member member, String reason, User issuer) {
        Punishment punishment = MySQLPunishment.create(member.user())
                                               .reason(reason).issuedBy(issuer)
                                               .build();

        SincBot.instance().punishmentManager().addPunishment(punishment);

        EmbedBuilder embedBuilder = new EmbedBuilder();

        SincBot.instance().modLogChannel().sendMessage("");
    }
}
