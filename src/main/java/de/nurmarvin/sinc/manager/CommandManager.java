package de.nurmarvin.sinc.manager;

import com.mewna.catnip.entity.message.Message;
import de.nurmarvin.sinc.command.Command;
import org.apache.commons.lang3.NotImplementedException;

import java.util.Map;

public interface CommandManager {
    default void handle(Message message) {
        throw new NotImplementedException("Command Manager does not implement CommandManager#handle");
    }

    Map<String, Command> commands();

    void registerCommand(Command command);
}
