package de.nurmarvin.sinc.manager;

import com.github.benmanes.caffeine.cache.Cache;
import de.nurmarvin.sinc.profile.Punishment;

import java.util.List;
import java.util.function.Consumer;

public interface PunishmentManager {
    List<Punishment> punishmentsById(long userId);

    void punishmentsByIdAsync(long userId, Consumer<List<Punishment>> punishments);

    boolean hasSettings(long userId);

    Cache<Long, List<Punishment>> punishmentsCache();

    void addPunishment(Punishment punishment);
}
