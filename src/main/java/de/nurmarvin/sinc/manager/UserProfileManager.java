package de.nurmarvin.sinc.manager;

import com.github.benmanes.caffeine.cache.Cache;
import de.nurmarvin.sinc.profile.UserProfile;

import java.util.function.Consumer;

public interface UserProfileManager {
    UserProfile userProfileById(long userId);

    void userProfileByIdAsync(long userId, Consumer<UserProfile> userProfile);

    boolean hasProfile(long userId);

    Cache<Long, UserProfile> userProfileCache();

    void upload(UserProfile userProfile);
}
