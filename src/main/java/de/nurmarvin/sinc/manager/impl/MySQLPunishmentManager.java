package de.nurmarvin.sinc.manager.impl;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import de.nurmarvin.sinc.SincBot;
import de.nurmarvin.sinc.manager.PunishmentManager;
import de.nurmarvin.sinc.profile.Punishment;
import de.nurmarvin.sinc.profile.impl.MySQLPunishment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;
import java.util.function.Consumer;

public final class MySQLPunishmentManager implements PunishmentManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(PunishmentManager.class);

    private Dao<Punishment, Long> punishmentsDao;
    private Cache<Long, List<Punishment>> punishmentsCache;

    public MySQLPunishmentManager() {
        this.punishmentsDao = SincBot.instance().mySQLManager().createDao(MySQLPunishment.class);
        this.punishmentsCache = Caffeine.newBuilder().weakValues().maximumSize(1000).build();
    }

    @Override
    public List<Punishment> punishmentsById(long userId) {
        List<Punishment> cachedPunishments = this.punishmentsCache.getIfPresent(userId);

        if(cachedPunishments != null) return cachedPunishments;

        QueryBuilder<Punishment, Long> statementBuilder = this.punishmentsDao.queryBuilder();
        try {
            statementBuilder.where().idEq(userId);
            List<Punishment> punishments = this.punishmentsDao.query(statementBuilder.prepare());

            this.punishmentsCache.put(userId, punishments);

            return punishments;
        }
        catch (Exception ex) {
            LOGGER.error("Error while searching for punishments for user id {}", userId, ex);
        }
        return null;
    }

    @Override
    public void punishmentsByIdAsync(long userId, Consumer<List<Punishment>> userSettings) {
        new Thread(() -> userSettings.accept(this.punishmentsById(userId))).start();
    }

    @Override
    public boolean hasSettings(long userId) {
        return false;
    }

    @Override
    public Cache<Long, List<Punishment>> punishmentsCache() {
        return punishmentsCache;
    }

    @Override
    public void addPunishment(Punishment punishment) {
        try {
            this.punishmentsDao.create(punishment);
            LOGGER.debug("Created new punishment for {}", punishment.userId());
        } catch (SQLException e) {
            LOGGER.error(String.format("Error while uploading new punishment for %s", punishment.userId()), e);
        }
    }
}
