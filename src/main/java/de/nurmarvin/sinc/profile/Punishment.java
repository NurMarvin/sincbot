package de.nurmarvin.sinc.profile;

public interface Punishment {
    long id();
    long userId();
    PunishmentType punishmentType();
    long duration();
    String reason();
    long issuedBy();
    boolean removed();
    String removeReason();
    long removedBy();

    enum PunishmentType {
        BAN,
        JAIL,
        MUTE,
        WARN
    }
}
