package de.nurmarvin.sinc.profile;

public interface UserProfile {
    String username();
    String discriminator();
    String avatarUrl();
    long id();
}
