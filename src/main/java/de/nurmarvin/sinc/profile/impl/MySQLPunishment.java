package de.nurmarvin.sinc.profile.impl;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mewna.catnip.entity.user.User;
import de.nurmarvin.sinc.profile.Punishment;

@DatabaseTable(tableName = "Punishments")
public final class MySQLPunishment implements Punishment {
    @DatabaseField(generatedId = true, unique = true)
    private long id;
    @DatabaseField
    private long userId;
    @DatabaseField(dataType = DataType.ENUM_TO_STRING)
    private PunishmentType punishmentType;
    @DatabaseField(canBeNull = false)
    private long duration;
    @DatabaseField(canBeNull = false)
    private String reason;
    @DatabaseField(canBeNull = false)
    private long issuedBy;
    @DatabaseField
    private String removeReason;
    @DatabaseField
    private long removedBy;

    /**
     * Used by OrmLite to create a new instance of {@link Punishment} to then populate it
     */
    @SuppressWarnings("unused")
    public MySQLPunishment() {
    }

    @Override
    public long id() {
        return 0;
    }

    public long userId() {
        return userId;
    }

    @Override
    public PunishmentType punishmentType() {
        return punishmentType;
    }

    @Override
    public long duration() {
        return duration;
    }

    @Override
    public String reason() {
        return reason;
    }

    @Override
    public long issuedBy() {
        return issuedBy;
    }

    public void remove(long userId, String reason) {
        this.removedBy = userId;
        this.removeReason = reason;
    }

    @Override
    public boolean removed() {
        return removeReason != null || removedBy > 0;
    }

    @Override
    public String removeReason() {
        return removeReason;
    }

    @Override
    public long removedBy() {
        return removedBy;
    }

    public static Builder create(User user) {
        return create(user.idAsLong());
    }

    public static Builder create(long userId) {
        return new Builder(userId);
    }

    public static class Builder {
        private MySQLPunishment mySQLPunishment;

        Builder(long userId) {
            this.mySQLPunishment = new MySQLPunishment();
            this.mySQLPunishment.userId = userId;
            this.mySQLPunishment.removedBy = -1;
            this.mySQLPunishment.reason = "";
            this.mySQLPunishment.punishmentType = PunishmentType.WARN;
        }

        public Builder issuedBy(User user) {
            return this.issuedBy(user.idAsLong());
        }

        public Builder punishmentType(PunishmentType punishmentType) {
            this.mySQLPunishment.punishmentType = punishmentType;
            return this;
        }

        public Builder issuedBy(long userId) {
            this.mySQLPunishment.issuedBy = userId;
            return this;
        }

        public Builder reason(String reason) {
            this.mySQLPunishment.reason= reason;
            return this;
        }

        public MySQLPunishment build() {
            return this.mySQLPunishment;
        }
    }
}
