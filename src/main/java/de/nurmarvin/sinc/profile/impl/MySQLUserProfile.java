package de.nurmarvin.sinc.profile.impl;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.mewna.catnip.entity.user.User;
import de.nurmarvin.sinc.profile.UserProfile;

@DatabaseTable(tableName = "UserProfiles")
public final class MySQLUserProfile implements UserProfile {
    @DatabaseField(canBeNull = false)
    private String username;
    @DatabaseField(canBeNull = false)
    private String discriminator;
    @DatabaseField(canBeNull = false)
    private String avatarUrl;
    @DatabaseField(id = true, unique = true, canBeNull = false)
    private long id;
    /**
     * Used by OrmLite to create a new instance of {@link UserProfile} to then populate it
     */
    @SuppressWarnings("unused")
    public MySQLUserProfile() {
    }

    public MySQLUserProfile(User user) {
        this.username = user.username();
        this.discriminator = user.discriminator();
        this.avatarUrl = user.effectiveAvatarUrl();
        this.id = user.idAsLong();
    }

    /**
     * The last known username of the user that owns this user profile, in most cases equal to {@link User#username()}
     * @return the username
     */
    @Override
    public String username() {
        return username;
    }

    public void username(String username) {
        this.username = username;
    }

    @Override
    public String discriminator() {
        return discriminator;
    }

    public void discriminator(String discriminator) {
        this.discriminator = discriminator;
    }

    @Override
    public String avatarUrl() {
        return avatarUrl;
    }

    public void avatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Override
    public long id() {
        return id;
    }

    public void id(long id) {
        this.id = id;
    }
}
