package de.nurmarvin.sinc.utils;

import com.mewna.catnip.entity.builder.EmbedBuilder;
import com.mewna.catnip.entity.guild.Member;
import com.mewna.catnip.entity.user.User;
import com.mewna.catnip.util.CatnipMeta;
import de.nurmarvin.sinc.SincBot;
import de.nurmarvin.sinc.command.CommandContext;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.awt.*;

public final class Embeds {
    public static void usage(CommandContext context) {
        StringBuilder description = new StringBuilder()
                .append(context.command().description())
                .append("\n\n`")
                .append(context.command().usage());

        context.command().subCommands().values().forEach(command -> description.append("\n").append(command.usage()));
        description.append("`");

        EmbedBuilder usage = errorEmbed(context.user())
                .title("Command Usage: " + context.command().name())
                .description(description.toString());
        context.send(usage.build());
    }

    public static void commandException(CommandContext context, Exception e) {
        EmbedBuilder usage = errorEmbed(context.user())
                .description(e.getMessage());
        context.send(usage.build());
    }

    public static void exception(CommandContext context, Exception e) {
        String stackTrace = ExceptionUtils.getStackTrace(e);
        EmbedBuilder usage = errorEmbed(context.user())
                .description(stackTrace.substring(0, Math.min(2048, stackTrace.length())));
        context.send(usage.build());
    }

    public static EmbedBuilder errorEmbed(User sender) {
        return new EmbedBuilder()
                .color(Settings.instance().botSettings().errorColor())
                .title("Error")
                .footer(SincBot.instance().settings().botSettings().botName() + " " + SincBot.VERSION + " " +
                        "by NurMarvin#1337 running on Catnip " + CatnipMeta.VERSION, sender.effectiveAvatarUrl());
    }

    public static EmbedBuilder normalEmbed(Member member) {
        return new EmbedBuilder()
                .color(normalColor(member))
                .footer(SincBot.instance().settings().botSettings().botName() + " " + SincBot.VERSION + " " +
                        "by NurMarvin#1337 running on Catnip " + CatnipMeta.VERSION,
                        member.user().effectiveAvatarUrl());
    }

    public static Color normalColor(Member member) {
        return member != null && member.color() != null ? member.color() :
               Settings.instance().botSettings().normalColor();
    }
}
