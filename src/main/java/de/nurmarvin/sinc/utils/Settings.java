package de.nurmarvin.sinc.utils;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.mewna.catnip.entity.user.Presence;
import de.nurmarvin.sinc.SincBot;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;

public class Settings {

    @SerializedName("debug")
    private boolean debug = false;

    @SerializedName("connection")
    private ConnectionSettings connectionSettings = new ConnectionSettings();

    @SerializedName("bot")
    private BotSettings botSettings = new BotSettings();

    @SerializedName("guild")
    private GuildSettings guildSettings = new GuildSettings();

    @SerializedName("apiKeys")
    private HashMap<String, String> apiKeys = Maps.newHashMap();

    public class ConnectionSettings {
        @SerializedName("token")
        private String token = "";

        @SerializedName("mysqlHost")
        private String mysqlHost = "localhost";

        @SerializedName("mysqlDatabase")
        private String mysqlDatabase = "sinc";

        @SerializedName("mysqlUser")
        private String mysqlUser = "username";

        @SerializedName("mysqlPassword")
        private String mysqlPassword = "password";

        public String token() {
            return token;
        }

        public String mysqlHost() {
            return mysqlHost;
        }

        public String mysqlDatabase() {
            return mysqlDatabase;
        }

        public String mysqlUser() {
            return mysqlUser;
        }

        public String mysqlPassword() {
            return mysqlPassword;
        }
    }

    public class BotSettings {
        private String botName = "Sinc";

        private String owner = "562415519454461962";

        private String prefix = "!";

        private Presence.ActivityType activityType = Presence.ActivityType.PLAYING;

        @JsonAdapter(ColorTypeAdapter.class)
        private Color normalColor = new Color(255, 71, 87);

        @JsonAdapter(ColorTypeAdapter.class)
        private Color errorColor = new Color(231, 76, 60);

        public String botName() {
            return botName;
        }

        public String owner() {
            return owner;
        }

        public String prefix() {
            return prefix;
        }

        public Presence.ActivityType activityType() {
            return activityType;
        }

        public Color normalColor() {
            return normalColor;
        }

        public Color errorColor() {
            return errorColor;
        }
    }

    public class GuildSettings {
        private String guildId = "567204357737676801";

        private String commentsChannelId = "575807247909650432";
        private ArrayList<String> imageChannelIds = Lists.newArrayList("575807275491393536");
        private String proofChannelId = "575807399936524315";
        private String modLogChannelId = "575807427329654784";

        private String mutedRoleId = "";

        public String guildId() {
            return guildId;
        }

        public String commentsChannelId() {
            return commentsChannelId;
        }

        public ArrayList<String> imageChannelIds() {
            return imageChannelIds;
        }

        public String proofChannelId() {
            return proofChannelId;
        }

        public String modLogChannelId() {
            return modLogChannelId;
        }

        public String mutedRoleId() {
            return mutedRoleId;
        }
    }

    public static Settings instance() {
        return SincBot.instance().settings();
    }

    public ConnectionSettings connectionSettings() {
        return connectionSettings;
    }

    public BotSettings botSettings() {
        return botSettings;
    }

    public GuildSettings guildSettings() {
        return guildSettings;
    }

    public HashMap<String, String> apiKeys() {
        return apiKeys;
    }

    public boolean debug() {
        return debug;
    }

    public void debug(boolean debug) {
        this.debug = debug;
    }
}
